module gitlab.com/gitlab-com/runbooks/pingdom

go 1.21

require (
	github.com/russellcardullo/go-pingdom v1.3.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
